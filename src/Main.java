import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {

        LinkedList<Integer> listaEnlazada = new LinkedList<>();


        listaEnlazada.add(1);
        listaEnlazada.add(2);
        listaEnlazada.add(3);

        System.out.println("Lista Enlazada:" + listaEnlazada);

        listaEnlazada.add(1, 4);
        System.out.println("Agrego elemento  4 en la posicion 1:" + listaEnlazada);


        int elemento = listaEnlazada.get(2);
        System.out.println("Elemento de la posicion 2:" + elemento);

        listaEnlazada.remove(0);
        System.out.println("elimino elemento de la posicion 0:" + listaEnlazada);


        boolean estaVacia = listaEnlazada.isEmpty();
        System.out.println("la lista esta vacias?: " + estaVacia);


        int tamayoLista = listaEnlazada.size();
        System.out.println("el tamaño de la lista es: " + tamayoLista);



    }
}